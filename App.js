
import React from 'react';
import { StatusBar } from 'expo-status-bar';

import { useFonts } from 'expo-font'
import AppLoading from 'expo-app-loading';

import HangmanNavigator from './src/hangman/hangmanNavigator';
import { Provider } from 'react-redux';
import store from './src/store';

export default function App() {
  const [loaded]= useFonts({
    gochi:require('./assets/fonts/Gochi_Hand/GochiHand-Regular.ttf')
  })

  if (!loaded) return <AppLoading/>

  //<StatusBar style="auto" />
  return (
    <Provider store={store}>
      <HangmanNavigator />
    </Provider>   
  );
}