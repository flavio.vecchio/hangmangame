# hangmanGame

Aplicacion creada para el curso de react native de coder house. 2021-10-24

# DOCUMENTACION

Leer el archivo HANGMAN.PDF que se encuentra en el raiz del repositorio para un detalle de la aplicacion.

# INSTALACION DE LA APK COMPILADA

En el directorio /apkBuilded se encuentra el archivo HANGMANGAME.APK que debe instalarse manualmente en el dispositivo.

# Creacion de la aplicacion con expo:
    expo init hangmanGame

# Ejecucion
    cd hangmanGame
    yarn start

# Caso particular
    Como uso nvm como manager para usar distintas versiondes de node tuve que ejecutar:

    nvm use --delete-prefix v14.17.3

# Algunos comandos de expo:
    › Press a │ open Android
    › Press i │ open iOS simulator
    › Press w │ open web

    › Press r │ reload app
    › Press m │ toggle menu
    › Press d │ show developer tools
    › shift+d │ toggle auto opening developer tools on startup (enabled)

    › Press ? │ show all commands

# Para usar REACT NAVIGATION sobre EXPO
    › yarn add @react-navigation/native
    › expo install react-native-screens react-native-safe-area-context
    › expo install @react-navigation/native-stack
    › yarn add @react-navigation/bottom-tabs

# Para usar REDUX sobre EXPO
    › yarn add redux react-redux
    › Para usar REACTtoTRON: yarn add reactotron-react-native
    › yarn add @react-native-community/async-storage
    › yarn add reactotron-redux  
    › yarn add redux-thunk  

# Uso de la CAMARA
    › yarn add expo-image-picker expo-file-system

# Integrar SQLITE
   › yarn add expo-sqlite

# ¿Por que integrar SQLITE?

SQLITE se integra en la aplicacion como un apoyo a su funcionamiento para soportar la capacidad de prestar servicio de manera OFF LINE.
En particular en esta aplicacion se trabaja en dual grabando informacion tanto en FIREBASE como en SQLITE, cuando hay que recuperar dicha informacion se compara para ver cual es la mas actual y tomar la valida, en caso que la configuracion mas actual sea la de FB se procedera a actualizar la informacion en SQLITE, si la informacion mas actualizada es la de SQLITE se procedera a actualizar FB.

# Build del proyecto

expo build:android -t apk

# Vista final

**Pantalla principal del juego donde podemos ingresar o poder crear una cuenta <br />**
[<img src="./screenshots/desafio8-1.png" width="350"/>](desafio8-1.png)<br />
<br />
**Pantalla donde podemos crear una cuenta agregando nuestro mail, clave y una foto <br />**
[<img src="./screenshots/desafio8-2.png" width="350"/>](desafio8-2.png)<br />
<br />
**Pantalla con los datos completos listos para impactar FIREBASE y SQLITE <br />**
[<img src="./screenshots/desafio8-3.png" width="350"/>](desafio8-3.png)<br />
<br />
**Vista de FIREBASE AUTHENTICATION vacio <br />**
[<img src="./screenshots/desafio8-4.png" width="640"/>](desafio8-4.png)<br />
<br />
**Vista de FIREBASE REAL TIME DATABASE vacia <br />**
[<img src="./screenshots/desafio8-5.png" width="640"/>](desafio8-5.png)<br />
<br />
**Luego de impactar los datos vemos la base con la informacion del usuario <br />**
[<img src="./screenshots/desafio8-6.png" width="640"/>](desafio8-6.png)<br />
<br />
**Luego de impactar los datos vemos la info de autenticacion en FIREBASE <br />**
[<img src="./screenshots/desafio8-7.png" width="640"/>](desafio8-7.png)<br />
<br />
**Vista de la base de datos SQLITE con la informacion guardad en ella <br />**
[<img src="./screenshots/desafio8-8.png" width="640"/>](desafio8-8.png)<br />
<br />
**Pantalla de bienvenida con los datos cargados listos para loguearme <br />**
[<img src="./screenshots/desafio8-9.png" width="350"/>](desafio8-9.png)<br />
<br />
**REDUX inicializado sin informacion <br />**
[<img src="./screenshots/desafio8-10.png" width="640"/>](desafio8-10.png)<br />
<br />
**REDUX ya con la informacion cargada luego de realizar el login <br />**
[<img src="./screenshots/desafio8-11.png" width="640"/>](desafio8-11.png)<br />
<br />
