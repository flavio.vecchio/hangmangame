# Desafio FINAL - V.10.0.0
DESAFIO FINAL

    >> Consigna: Integrar conocimientos de la cursada
    Ver HANGMAN.PDF para todo el detalle 

# Desafio 8 - V.8.0.0
DEVICE FEATURES 2

    >> Consigna: Integrar interfaces del dispositivo y sincronización offline 
        Integrar sqlite para sincronización offline
        Utilizar interfaces del dispositivo como la cámara


# Desafio 7 - V.7.0.0
DEVICE FEATURES

    Consigna: Continuando con tu proyecto, ahora debes integrar una interfaz o característica del dispositivo, recomendamos location o cámara
        Integrar características del dispositivo (location o cámara)
        Aplicar los permisos correspondientes
        Manejar el caso de permisos negados


# Desafio 6 - V.6.0.0
INTEGRACION CON REDUX

    Consigna: A partir de tu app en desarrollo, crear paso a paso las funcionalidades:
        Configurar e integrar redux
        Manejo del estado de la aplicación


# Desafio 5 - V.5.0.0
TAB NAVIGATION

    Consigna: Agregar la navegación de tabs:
        Uso de navegación por pestañas, usando cualquiera de los componentes tab de React Navigation (recomendado Bottom Tabs)
        Uso de las opciones de configuración de pantallas de React Navigation


# Desafio 4 - V.4.0.0
VISTAS & NAVEGACION

    Consigna: A partir de tu app en desarrollo, crear paso a paso:
        Configurar rutas usando React Navigation.
        Navegar entre ellas.


# Desafio 3 - V.3.0.0
SWITCH & ELEMENTOS EXTERNOS

    Consigna: A partir de este ciclo de estilos y elementos, integra a tu proyecto un poco más de comportamiento lógico y visual:
        Renderizado condicional entre elementos (switch entre pantallas)
        Constantes compartidas.
        Fonts externos.


# Desafio 2 - V.2.0.0
    Lista optimizada

    A partir de este ciclo inicial, crear paso a paso una pequeña lista optimizada
con las funcionalidades y estilos más concretos:

    ● Agregar acciones a los ítems, como agregar, borrar o marcar como completado
    ● Estilo visual de la app


# Desafio 1 - V.1.0.0
    Crear aplicación Expo

    Formato: Proyecto React Native nombrado como “App+Apellido”. 
    Sugerencia: Intenta agregar un texto adicional

    Consigna: Crear una aplicación con Expo CLI con el nombre de tu proyecto, ejecutando los comandos necesarios para instalar las dependencias, configurarlo y visualizarlo en tu dispositivo móvil o emulador.

    Modifica el texto de la pantalla inicial a "Hola, Coder!"

