import * as SQLite from 'expo-sqlite';

const db = SQLite.openDatabase('hangman.db');

export const init = () => {
  const promise = new Promise((resolve, reject) => {
    db.transaction((tx) => {
      tx.executeSql(
        `CREATE TABLE IF NOT EXISTS hangman (
          id INTEGER PRIMARY KEY NOT NULL,
          userId TEXT NOT NULL,
          userName TEXT NOT NULL,
          avatar TEXT NOT NULL,
          version INT NOT NULL
        )`,
        [],
        () => { resolve() },
        (_, err) => { reject(err) },
      );
    });
  });

  return promise;
}

export const insertConfig = (
  userId,
  userName,
  avatar,
  version
) => {
  return new Promise((resolve, reject) => {
    db.transaction(tx => {
      tx.executeSql(
        `INSERT INTO hangman (userId, userName, avatar, version)
          VALUES (?, ?, ?, ?)`,
        [userId, userName, avatar, version],
        (_, result) => resolve(result),
        (_, err) => reject(err),
      )
    })
  })
}

export const fetchConfig = (userId) => {
  return new Promise((resolve, reject) => {
    db.transaction(tx => {
      tx.executeSql(
        `SELECT * FROM hangman WHERE userId = '${userId}'`,
        [],
        (_, result) => resolve(result),
        (_, err) => reject(err),
      )
    })
  })
}