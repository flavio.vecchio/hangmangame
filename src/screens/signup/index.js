import React, { useState, useEffect } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  KeyboardAvoidingView,
  TextInput,
} from 'react-native';
import { useDispatch } from 'react-redux';
import { signupNewUser } from '../../store/actions/userData.action';

import ImageSelector from '../../components/imageSelector';
import { Feather } from '@expo/vector-icons';
import styles from './styles'
import SignupLayout from './layout';


const Signup = () => {
  const dispatch = useDispatch();
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const [image, setImage] = useState('');

  const handlePickImage = (image) => {
    setImage(image);
  }

  

  const handleConfirm = async () => {
    await dispatch(signupNewUser(email,password, image))
  }

  const [viewPassword, setViewPassword] = useState({hide: true, icon: 'eye-off'});

  const handleViewPassword = () => {
    if (viewPassword.hide)
    {
      setViewPassword({hide: false, icon: 'eye'})
    }else
    {
      setViewPassword({hide: true, icon: 'eye-off'})
    }
  }
  
  return (
    <SignupLayout
      handlePickImage={handlePickImage}
      email={email}
      setEmail={setEmail}
      viewPassword={viewPassword}
      password={password}
      setPassword={setPassword}
      handleViewPassword={handleViewPassword}
      handleConfirm={handleConfirm}
    />
  );
};



export default Signup;