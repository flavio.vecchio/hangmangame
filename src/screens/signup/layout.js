import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  KeyboardAvoidingView,
  TextInput,
} from 'react-native';


import ImageSelector from '../../components/imageSelector';
import { Feather } from '@expo/vector-icons';
import styles from './styles'



const SignupLayout = ({
    handlePickImage,
    email,
    setEmail,
    viewPassword,
    password,
    setPassword,
    handleViewPassword,
    handleConfirm,
}) => {
  
  
  return (
    <KeyboardAvoidingView style={styles.screen}>
      <View style={styles.container}>
        <Text style={styles.title}>
          Nuevo Usuario
        </Text>

        <ImageSelector onImage={handlePickImage} />

        <Text style={styles.label}>Email</Text>
        <TextInput
          style={styles.inputEmail}
          keyboardType="email-address"
          autoCapitalize="none"
          value={email}
          onChangeText={setEmail}
        />
        <Text style={styles.label}>Clave</Text>
        <View style={styles.inputContainer}>
          <TextInput
            style={styles.inputPassword}
            secureTextEntry={viewPassword.hide}
            autoCapitalize="none"
            value={password}
            onChangeText={setPassword}
          />
          <TouchableOpacity onPress={handleViewPassword} style={styles.searchIcon}>
            <Feather name={viewPassword.icon} size={20} color="black" />
          </TouchableOpacity>
         

        </View>
        <TouchableOpacity onPress={handleConfirm}>
          <Text style={styles.confirmButton}>
            Registrarse
          </Text>
        </TouchableOpacity>
      </View>
    </KeyboardAvoidingView>
  );
};



export default SignupLayout;