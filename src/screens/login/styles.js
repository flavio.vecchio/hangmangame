import { StyleSheet } from 'react-native';
import Colors from '../../constants/colors';

export default StyleSheet.create({

    inputContainer: {
        flexDirection: 'row',
        borderBottomWidth: 1,
        borderColor: '#000',
        paddingBottom: 10,
      },
      inputPassword: {
        flex: 1,
        paddingHorizontal: 2,
        paddingVertical: 5,
        borderBottomColor: '#ccc',
        borderBottomWidth: 1,
      },
      inputEmail: {
        paddingHorizontal: 2,
        paddingVertical: 5,
        borderBottomColor: '#ccc',
        borderBottomWidth: 1,
      },
      searchIcon: {
        paddingLeft:10,    
      },
      screen: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.blue,
      },
      title: {
        fontSize: 24,
        marginBottom: 18,
        textAlign: 'center',
        fontFamily: 'gochi',
      },
      container: {
        width: '80%',
        maxWidth: 400,
        padding: 12,
        margin: 12,
        borderColor: '#ccc',
        borderWidth: 1,
        borderRadius: 10,
        backgroundColor: 'white',
      },
      prompt: {
        alignItems: 'center',
      },
      promptMessage: {
        fontSize: 16,
        color: '#333',
      },
      promptButton: {
        fontSize: 16,
        color: Colors.primary,
      },
      label: {
        marginVertical: 8,
      },
      
      confirmButton: {
        fontSize: 30,
        textAlign: 'center',
        marginVertical: 10,
        paddingVertical: 5,
        backgroundColor: Colors.primary,
        color: 'white',
        fontFamily: 'gochi',
      }

      
});
