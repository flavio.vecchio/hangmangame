import React, { useState, useEffect } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  KeyboardAvoidingView,
  TextInput,
  Alert,
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { signIn, hideLoginFail } from '../../store/actions/userData.action';
import { Feather } from '@expo/vector-icons';
import { init as initDB } from '../../db';
import styles from './styles'


const Login = ({navigation}) => {
  const dispatch = useDispatch();
  const [email, setEmail] = useState('flavito@flavito.com');
  const [password, setPassword] = useState('flavito');
  var showLogginFail = useSelector(state => state.UserState.showLogginFail);
  var errorMessage = useSelector(state => state.UserState.errorMessage);

  useEffect(() => {
    initialize();
  });

  const initialize = () => {
    initDB()
      .then(() => console.log('Database initialized'))
      .catch(err => {
        console.log('Database failed to connect')
        console.log(err.message)
      })
  }

  

  const handleConfirm = async () => {
    await dispatch(signIn(email,password))
    dispatch(hideLoginFail());
  }

  const handleToggleSignUp = () => {
    navigation.navigate('Signup');
  }

  const [viewPassword, setViewPassword] = useState({hide: true, icon: 'eye-off'});
  const handleViewPassword = () => {
    if (viewPassword.hide)
    {
      setViewPassword({hide: false, icon: 'eye'})
    }else
    {
      setViewPassword({hide: true, icon: 'eye-off'})
    }
  }

  return (
    <KeyboardAvoidingView style={styles.screen}>
      <View style={styles.container}>
        <Text style={styles.title}>
          Bienvenido al juego del ahorcado
        </Text>

        <Text style={styles.label}>Email</Text>
        <TextInput
          style={styles.inputEmail}
          keyboardType="email-address"
          autoCapitalize="none"
          value={email}
          onChangeText={setEmail}
        />
        <Text style={styles.label}>Clave</Text>
        <View style={styles.inputContainer}>
          <TextInput
            style={styles.inputPassword}
            secureTextEntry={viewPassword.hide}
            autoCapitalize="none"
            value={password}
            onChangeText={setPassword}
          />
          <TouchableOpacity onPress={handleViewPassword} style={styles.searchIcon}>
            <Feather name={viewPassword.icon} size={20} color="black" />
          </TouchableOpacity>
         

        </View>
        
        <TouchableOpacity onPress={handleConfirm}>
          <Text style={styles.confirmButton}>
              Ingresar
          </Text>
        </TouchableOpacity>
        <View style={styles.prompt}>
          <Text>
             ¿Aún no estás registrado?
          </Text>
          <TouchableOpacity onPress={handleToggleSignUp} style={styles.promptMessage}>
            <Text style={styles.promptButton}>
                Registrarse
            </Text>
          </TouchableOpacity>
        </View>
      </View>
      {showLogginFail && Alert.alert(
          'Ingreso fallido',
          (errorMessage && errorMessage.length >0) ? errorMessage:'Usuario o clave incorrecta',
          [{ text: 'Reintentar...' }],
        ) }
    </KeyboardAvoidingView>
  );
};



export default Login;