
import React, { useEffect } from 'react';
import { View, Button,  BackHandler} from 'react-native';


import { useDispatch } from 'react-redux';
import { 
    singout
} from '../../store/actions/userData.action';

import styles from './styles'


export function title() {
    return 'Perfil de usuario';
  }

export default function Game({ navigation }) {

    const dispatch = useDispatch();

    const handleBackButtonClick = () => {
        dispatch(clearTimer());
    }

    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);

        
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButtonClick);
            
        }
    }, [])

    
    const logoutHandler = () => {
        dispatch(singout());
    }
    
    
    //Vista
    return (
        <View style={styles.container} >
            <View style={styles.buttonContainer} >
                <Button title="Cerrar sesion" onPress={logoutHandler} /> 
            </View>
        </View>
    );
}
