import { StyleSheet } from 'react-native';
import Colors from '../../constants/colors'

export default StyleSheet.create({

    timerContainer: {
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
      },

    container: {
        flex: 1,
        backgroundColor: Colors.white,
        paddingTop: 10,
    },
    buttonContainer: {
        flexDirection: 'column',
        marginBottom: 20,
        paddingRight: 20,
        paddingLeft: 10,
        paddingTop: 20,
    },
    title: {
        flexDirection: 'row',
        fontSize: 20,
        marginVertical: 10,
        marginLeft: 5,
        fontFamily: 'gochi',
        justifyContent: 'flex-end', 
    },
    containerLogo: {
        paddingTop: 50,
        justifyContent: 'center',
        alignItems: 'center',
    },
    logo: {
        width: 130,
        height: 170,
    },
    cabeza: {
        width: 40,
        height: 40,
        position: 'absolute',
        top:72,
        left:208,
    },
    tronco: {
        width: 20,
        height: 60,
        position: 'absolute',
        top:110,
        left:220,
    },
    brazoDerecho: {
        width: 30,
        height: 40,
        position: 'absolute',
        top:110,
        left:240,
    },
    brazoIzquierdo: {
        width: 30,
        height: 40,
        position: 'absolute',
        top:113,
        left:190,
    },
    pieDerecho: {
        width: 30,
        height: 40,
        position: 'absolute',
        top:170,
        left:230,
    },
    pieIzquierdo: {
        width: 33,
        height: 45,
        position: 'absolute',
        top:162,
        left:195,
    },
    containerWord: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginLeft: 10,
        marginRight: 10,
    },
    containerLetter: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        position: 'absolute',
        marginLeft: 10,
        paddingTop: 20,
    },
    letterPosition: {
        width: 30,
        height: 10,
    },
    letter: {
        fontSize: 50,
        fontFamily: 'gochi',
    },
    letterList: {
        fontSize: 100,
        fontFamily: 'gochi',
        
    },
    letterButton:{
        margin: 5,
        backgroundColor: 'lightblue',
        shadowColor: 'black',
        shadowOffset: {
            width: 0,
            height: 12,
        },
        shadowOpacity: 0.58,
        shadowRadius: 16.00,

        elevation: 24,
    },
    rectangle: {     
        width: 340,
        backgroundColor: 'lightgray',
        marginLeft:10,
        marginBottom:10,
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
      },

      
});
