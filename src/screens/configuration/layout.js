
import React from 'react';
import { View, Button } from 'react-native';
import CheckBox from 'react-native-check-box'
import styles from './styles'

function ConfigurationLayout({
    handleGroupOfCheckbox,
    usarPalabraSeleccionada,
    usarCualquierPalabra,
    switchToDictionary
}) {
  
  return (
    <View style={styles.container}>

      <View style={styles.checkboxContainer}>
          <CheckBox
              style={{paddingTop:10}}
              onClick={handleGroupOfCheckbox}
              isChecked={usarPalabraSeleccionada}
              leftText={"Usar palabras seleccionadas"}
          />
          <CheckBox
              style={{paddingTop:10}}
              onClick={handleGroupOfCheckbox}
              isChecked={usarCualquierPalabra}
              leftText={"Usar cualquiera de la lista"}
          />
      </View>
      <View style={styles.buttonContainer} >
        <Button title="Editar diccionario" onPress={switchToDictionary} /> 
      </View>
    </View>
  );
}

export default ConfigurationLayout;