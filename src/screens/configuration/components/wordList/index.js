
import React from 'react';
import {FlatList } from 'react-native';
import WordItem from '../wordItem';

function WordList({palabras, setCheckboxItemState, usarPalabraSeleccionada, deleteWord}) {
  
  return (
    <FlatList
        data={palabras}
        renderItem={data => (
        <WordItem
          data={data}
          setCheckboxItemState={setCheckboxItemState}
          usarPalabraSeleccionada={usarPalabraSeleccionada}
          deleteWord={deleteWord}
        />
        )}
        keyExtractor={(palabra) => palabra.id }
  />
  );
}

export default WordList;