
import React from 'react';
import {Text, View, Button } from 'react-native';
import CheckBox from 'react-native-check-box'
import styles from './styles'
import Colors from '../../../../constants/colors'

function WordItem({data, setCheckboxItemState, usarPalabraSeleccionada, deleteWord}) {
  
  return (
        <View style={styles.elementOfList}>
            <Text style={{width: 30}}>{data.item.id+' - '}</Text>
            <Text style={{width: 200}}>{data.item.palabra}</Text>
            <CheckBox
                onClick={() => (setCheckboxItemState(data.item.id))}
                isChecked={data.item.isChecked}
                disabled={usarPalabraSeleccionada?false:true}
                checkBoxColor={usarPalabraSeleccionada?Colors.black:Colors.gray}
            />
            <Button title="  -  "
                onPress={deleteWord.bind(this, data.item.id)} />
        </View>
  );
}

export default WordItem;