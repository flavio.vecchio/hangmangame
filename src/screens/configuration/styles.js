import { StyleSheet } from 'react-native';
import Colors from '../../constants/colors'

export default StyleSheet.create({
  
    
      listContainer:{
        flex: 1,
        flexDirection: 'column',
      },
      checkboxContainer:{
        flexDirection: 'column',
        marginBottom: 20,
        paddingRight: 20,
        paddingLeft:10,
        paddingTop:10,
      },
      inputContainer: {
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center',
        paddingRight: 20,
        paddingLeft:10,
      },
      textInput: {
        width:200,
        borderBottomColor:Colors.black,
        borderBottomWidth:1,
        backgroundColor: Colors.white,
        paddingLeft: 20,
      },
      title: {
        alignItems: 'center',
        padding: 30,
      },
      container: {
        flex: 1,
        backgroundColor: Colors.white,
        paddingLeft:10,
        paddingTop: 10,
      },
      buttonContainer:{
        flexDirection: 'column',
        marginBottom: 20,
        paddingRight: 20,
        paddingLeft:10,
        paddingTop:5,
      },

  });