
import React from 'react';
import ConfigurationLayout from './layout';
import { useDispatch, useSelector } from 'react-redux';
import { 
  updateUseAnyWord,
  updateUseSelectedWordOnly,
} from '../../store/actions/dictionary.action';

export function title() {
  return 'Configuracion del juego';
}

export default function Configuration({navigation}) {
  
    const dispatch = useDispatch();

    //State
    var usarPalabraSeleccionada = useSelector(state => state.Words.useSelectedWordOnly);
    var usarCualquierPalabra = useSelector(state => state.Words.useAnyWord);
    
    //Funciones
  const handleGroupOfCheckbox = () => {
    dispatch(updateUseSelectedWordOnly(!usarPalabraSeleccionada));
    dispatch(updateUseAnyWord(!usarCualquierPalabra));
  }

  const switchToDictionary = () => navigation.navigate("Configuration/Dictionary",{usarPalabraSeleccionada:usarPalabraSeleccionada,
    usarCualquierPalabra:usarCualquierPalabra });

    //Vista
  return (
    <ConfigurationLayout
        handleGroupOfCheckbox={handleGroupOfCheckbox}
        usarPalabraSeleccionada={usarPalabraSeleccionada}
        usarCualquierPalabra={usarCualquierPalabra}
        switchToDictionary={switchToDictionary}
    />
  );
}

