
import React from 'react';
import { View,TextInput, Button } from 'react-native';
import Confirm from '../../../../components/modal'
import styles from './styles'
import WordList from '../../components/wordList';

function DictionaryLayout({
    palabra,onHandlerChangeText,addWord,
    usarPalabraSeleccionada,
    palabras,
    setCheckboxItemState,
    deleteWord,
    modalVisible,
    palabraSeleccionada,
    handleConfirmDelete,
    handleCancelDelete
}) {
  
  return (
    <View style={styles.container}>

      <View style={styles.inputContainer}>
        <TextInput style={styles.textInput}
          placeholder="Ingrese una palabra"
          value={palabra}
          onChangeText={onHandlerChangeText}
        />
        <Button title="  +  "
          onPress={addWord} />
      </View>
      <View style={styles.listContainer}>
          <WordList
            palabras={palabras}
            setCheckboxItemState={setCheckboxItemState}
            usarPalabraSeleccionada={usarPalabraSeleccionada}
            deleteWord={deleteWord}
          />
      </View>
      <Confirm 
        modalVisible={modalVisible} 
        title='¿Está seguro que desea borrar?'
        palabraSeleccionada={palabraSeleccionada}
        handleConfirmDelete={handleConfirmDelete}
        handleCancelDelete={handleCancelDelete}
      />
        
    </View>
  );
}

export default DictionaryLayout;