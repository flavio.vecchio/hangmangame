
import React, { useState } from 'react';
import DictionaryLayout from './layout';
import { useDispatch, useSelector } from 'react-redux';
import { 
  addWord as addWordAction, 
  removeWord as removeWordAction, 
  updateWord,
} from '../../../../store/actions/dictionary.action';

export function title() {
  return 'Diccionario de palabras';
}

export default function Dictionary({navigation,route}) {
  
    const dispatch = useDispatch();

    //State
    const palabras = useSelector(state => state.Words.words);

    const [palabra, setPalabra] = useState('');
    const [modalVisible, setModalVisible] = useState(false);
    const [palabraSeleccionada, setPalabraSeleccionada] = useState({ id: 0, palabra:"", isChecked:false});
    const [contador, setContador] = useState(palabras.length)

    //Funciones
  const setCheckboxItemState = (id) => {
    const palabra = palabras.find( word => word.id === id);
    palabra.isChecked = !palabra.isChecked;
    dispatch(updateWord(palabra));
  }

  const onHandlerChangeText = (text) => {
    if(/^[A-Za-zñÑ]+$/.test(text) || text.length==0)
      setPalabra(text)
  }

  function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }

  const addWord = () => {
    if(palabra.length==0) return;
    //const colPalabras = [...palabras];
    const nuevaPalabra={ id: contador+1, palabra:capitalizeFirstLetter(palabra), isChecked:false}

    //colPalabras.push(nuevaPalabra);
    setPalabra('');
    setContador(contador+1);
    dispatch(addWordAction(nuevaPalabra));

  }

  const deleteWord = (id) => {
    setPalabraSeleccionada(palabras.find( w => w.id == id))
    setModalVisible(true);
  }

  const handleConfirmDelete = () => {
    dispatch(removeWordAction(palabraSeleccionada));
    setModalVisible(false);
    setPalabraSeleccionada({ id: 0, palabra:"", isChecked:false});
  }

  const handleCancelDelete = () => {
    setModalVisible(false);
    setPalabraSeleccionada({ id: 0, palabra:"", isChecked:false});
  }

  

    //Vista
  return (
    <DictionaryLayout
        palabra={palabra}
        onHandlerChangeText={onHandlerChangeText}
        addWord={addWord}
        usarPalabraSeleccionada={route.params.usarPalabraSeleccionada}
        usarCualquierPalabra={route.params.usarCualquierPalabra}
        palabras={palabras}
        setCheckboxItemState={setCheckboxItemState}
        deleteWord={deleteWord}
        modalVisible={modalVisible}
        palabraSeleccionada={palabraSeleccionada}
        handleConfirmDelete={handleConfirmDelete}
        handleCancelDelete={handleCancelDelete}
    />
  );
}

