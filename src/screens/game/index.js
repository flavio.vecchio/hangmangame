
import React, { useEffect, useState } from 'react';
import { Text, View, Image, FlatList, TouchableOpacity, BackHandler} from 'react-native';
import { useIsFocused } from "@react-navigation/native";

import styles from './styles'
import GameLayout from './layout';

import { abcdario as grupoDeLetras } from '../../constants/abcdario'
import DrawLetters from './components/drawLetters';
import SimpleCustomModal from '../../components/simpleModal';

import { useDispatch, useSelector } from 'react-redux';
import { 
  updateErrors,
  updateLettersGuessed,
  updateSeconds,
  updateStartTimer,
  updateWordToGuess,
  addOneMinute,
  addOneSecond,
  setTimer as setTimerAction,
  clearTimer,
  clearGame,
} from '../../store/actions/gameState.action';

function randomIntFromInterval(min, max) { // min and max included 
    return Math.floor(Math.random() * (max - min + 1) + min)
  }

export function title() {
    return 'Pantalla del juego';
  }

export default function Game({ navigation }) {
    const isFocused = useIsFocused();

    const dispatch = useDispatch();

    const palabraAAdivinar = useSelector(state => state.GameState.palabraAAdivinar);
    const letrasAdivinadas = useSelector(state => state.GameState.letrasAdivinadas);
    const errores = useSelector(state => state.GameState.errores);
    const segundos = useSelector(state => state.GameState.segundos);
    const minutos = useSelector(state => state.GameState.minutos);
    const startTimer = useSelector(state => state.GameState.startTimer);
    const timer = useSelector(state => state.GameState.timer);
    const [cantidadDeLetrasAdivinadas, setCantidadDeLetrasAdivinadas] = useState(0);
    const dictionary = useSelector(state => state.Words);

    const handleBackButtonClick = () => {
        dispatch(clearTimer());
    }

    useEffect(() => {
        //console.log("Se ejecuto useEffect");

        BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);

        if(startTimer && !timer){
            const aTimer = setInterval(tick.bind(this), 1000);
            dispatch(setTimerAction(aTimer));
        }
        if(palabraAAdivinar.length===0){
            
            var listWords = dictionary.words;
            if(dictionary.useSelectedWordOnly){
                listWords = dictionary.words.filter( (w) => w.isChecked);
            }
            if(listWords.length===0){ listWords = dictionary.words };
            
            const wordIndex = randomIntFromInterval(0,listWords.length-1);
            dispatch(updateWordToGuess(listWords[wordIndex].palabra));    

        }
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButtonClick);
            return (clearInterval(timer));
        }
    }, [isFocused,cantidadDeLetrasAdivinadas])

    const tick = () => {
            dispatch(addOneSecond());
    }

    const timeRender = () => {

        if(segundos===60){
            dispatch(updateSeconds(0));
            dispatch(addOneMinute());
        };

        return String(minutos).padStart(2, '0')+':'+String(segundos).padStart(2, '0')
    }

    const setFinalGame = (points) => {

        dispatch(clearGame());
        setCantidadDeLetrasAdivinadas(0);
        dispatch(clearTimer());
        
        navigation.goBack();
    };

    const winGame = () => {
        if(cantidadDeLetrasAdivinadas===palabraAAdivinar.length){
            clearInterval(timer);
            return true;
        }

        return false;
    }

    const lostGame = () => {
        if(errores===6){
            clearInterval(timer);
            return true;
        }

        return false;
    }

    const handleWin = () => {
        setFinalGame(+1);
    }

    const handleLost = () => {
        setFinalGame(-1);
    }

    const onPress = (letra) => {
        if(!startTimer){
            dispatch(updateStartTimer(!startTimer));
            const aTimer = setInterval(tick.bind(this), 1000);
            dispatch(setTimerAction(aTimer));
        }
        
        var adivino=false;

        var sumarLetra =0;
        palabraAAdivinar.split('').map( (l, index) => {
           if(l.toUpperCase().trim() === letra.toUpperCase().trim() ){
                adivino=true;
                if(!letrasAdivinadas[index])
                    sumarLetra=sumarLetra+1;
                dispatch(updateLettersGuessed(letra,index));
                if(letrasAdivinadas[index])
                return;
           }
        })
        if(!adivino) 
            dispatch(updateErrors(errores+1));
        
        setCantidadDeLetrasAdivinadas(cantidadDeLetrasAdivinadas+sumarLetra);

    }
    
    const dibujarPosicionDeLetras = () => {
        var posicionLetras = [];

        for (let i = 0; i < palabraAAdivinar.length; i++) 
        {
            posicionLetras.push(
                <Image key={i} style={styles.letterPosition} source={require('../../../assets/images/Hangman/posicionLetra.png')} />
            );
        }
        return posicionLetras;
    }

    const dibujarErrores = () => {
        var erroresRender = [];
        if(errores>=1){
            erroresRender.push(<Image key={1} style={styles.cabeza} source={require('../../../assets/images/Hangman/hg_cabeza.png')} />)
        }
        if(errores>=2){
            erroresRender.push(<Image key={2} style={styles.tronco} source={require('../../../assets/images/Hangman/hg_tronco.png')} />)
        }
        if(errores>=3){
            erroresRender.push(<Image key={3} style={styles.brazoDerecho} source={require('../../../assets/images/Hangman/hg_brazoDerecho.png')} />)
        }
        if(errores>=4){
            erroresRender.push(<Image key={4} style={styles.brazoIzquierdo} source={require('../../../assets/images/Hangman/hg_brazoIzquierdo.png')} />)
        }
        if(errores>=5){
            erroresRender.push(<Image key={5} style={styles.pieDerecho} source={require('../../../assets/images/Hangman/hg_pieDerecho.png')} />)
        }
        if(errores>=6){
            erroresRender.push(<Image key={6} style={styles.pieIzquierdo} source={require('../../../assets/images/Hangman/hg_pieIzquierdo.png')} />)
        }

        return erroresRender;
    }

    
    //Vista
    return (
        <GameLayout
            timeRender={timeRender}
            dibujarErrores={dibujarErrores}
            palabraAAdivinar={palabraAAdivinar}
            letrasAdivinadas={letrasAdivinadas}
            dibujarPosicionDeLetras={dibujarPosicionDeLetras}
            grupoDeLetras={grupoDeLetras}
            onPress={onPress}
            handleLost={handleLost}
            handleWin={handleWin}
            lostGame={lostGame}
            winGame={winGame}
        />
    );
}
