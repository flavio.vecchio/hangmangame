
import React from 'react';
import { Text } from 'react-native';
import styles from './styles'


function DrawLetters({letrasAdivinadas, palabraAAdivinar}) {
  
    var posicionLetras = [];
    var letraADibujar; 
    var margin = 54;

    for (let i = 0; i < palabraAAdivinar.length; i++) 
    {
        letraADibujar=letrasAdivinadas[i];
        
        if(i===0){
            margin=0;
        }else{
            margin=25-i;
        }

        if(!letraADibujar){
            letraADibujar='   ';
            margin=50;
        } 

        posicionLetras.push(<Text key={i} style={[styles.letter]}>{letraADibujar}</Text>)

    }

  return (
      posicionLetras
  );
}



export default DrawLetters;
