
import React from 'react';
import { Text, View, Image, FlatList, TouchableOpacity } from 'react-native';

import styles from './styles'
import DrawLetters from './components/drawLetters';
import SimpleCustomModal from '../../components/simpleModal';


export default function GameLayout({ 
    timeRender,
    dibujarErrores,
    palabraAAdivinar,
    letrasAdivinadas,
    dibujarPosicionDeLetras,
    grupoDeLetras,
    onPress,
    handleWin,
    handleLost,
    lostGame,
    winGame,
 }) {
    
    //Vista
    return (
        <View style={styles.container} >
            <View style={styles.title}>
                <Text>Tiempo: {timeRender()} </Text>
            </View>
            <View style={styles.containerLogo} >
                <Image style={styles.logo} source={require('../../../assets/images/Hangman/hg_horcaVacia.png')} />
                {dibujarErrores()}
            </View>
            <View style={styles.container} >
                <View style={styles.containerWord} >
                    <DrawLetters palabraAAdivinar={palabraAAdivinar} letrasAdivinadas={letrasAdivinadas}/>
                </View>
                <View style={styles.containerWord} >
                    {dibujarPosicionDeLetras()}
                </View>
            </View>
            <View style={styles.rectangle}>
                <FlatList
                    data={grupoDeLetras}
                    horizontal={true}
                    renderItem={data => (
                        <TouchableOpacity
                                onPress={onPress.bind(this,data.item.letter)}
                                style={styles.letterButton}
                            >
                                <Text style={styles.letterList}>{data.item.letter}</Text>
                        </TouchableOpacity>
                    )}
                    keyExtractor={(letra) => letra.id}
                />
            </View>
            {winGame()? 
                <SimpleCustomModal modalVisible={true} title="FELICITACIONES, GANASTE LA PARTIDA!!" handleOk={handleWin}/> : undefined
            }  

            {lostGame()? 
                <SimpleCustomModal modalVisible={true} title="LO SIENTO, PERDISTE LA PARTIDA!! :(" handleOk={handleLost}/> : undefined
            } 
              
        
        </View>
    );
}
