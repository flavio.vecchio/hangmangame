
import React from 'react';
import { Text, View, Button,Modal } from 'react-native';
import styles from './styles'

function CustomModal({modalVisible, title, palabraSeleccionada, handleConfirmDelete, handleCancelDelete}) {
  
  return (
      <Modal animationType="slide" visible={modalVisible} transparent>
        <View style={styles.modalContainer}>
          <View style={[styles.modalContent, styles.shadow]}>
            <Text style={styles.modalMessage}>{title}</Text>
            <Text style={styles.modalTitle}>{palabraSeleccionada.palabra}</Text>
            <View style={styles.modalButtons}>
            <View style={{paddingRight:10}}>
                <Button
                    onPress={handleConfirmDelete}
                    title="CONFIRMAR"
                />
            </View>
            <View style={{paddingLeft:10}}>
                <Button
                    onPress={handleCancelDelete}
                    title="CANCELAR"
                />
            </View>
            </View>
          </View>
        </View>
      </Modal>
  );
}



export default CustomModal;