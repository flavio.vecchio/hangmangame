
import React from 'react';
import { Text, View, Button,Modal } from 'react-native';
import styles from './styles'

function SimpleCustomModal({modalVisible, title, handleOk }) {
  
  return (
      <Modal animationType="slide" visible={modalVisible} transparent>
        <View style={styles.modalContainer}>
          <View style={[styles.modalContent, styles.shadow]}>
            <Text style={styles.modalMessage}>{title}</Text>
            <View style={styles.modalButtons}>
            <View style={{paddingRight:10}}>
                <Button
                    onPress={handleOk}
                    title="OK"
                />
            </View>
            </View>
          </View>
        </View>
      </Modal>
  );
}



export default SimpleCustomModal;