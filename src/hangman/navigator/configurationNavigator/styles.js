import { StyleSheet } from 'react-native';
import Colors from '../../../constants/colors'

export default StyleSheet.create({
  
    header: {
      backgroundColor: Platform.OS === 'android' ? Colors.blue : 'white',
    },
    container: {
      flex: 1,
      backgroundColor: Colors.white,
      paddingTop:40,
    },
    buttonContainer:{
      flexDirection: 'column',
      marginBottom: 20,
      paddingRight: 20,
      paddingLeft:10,
      paddingTop:20,
    },
    title:{
      fontSize: 20,
      marginVertical: 10,
      marginLeft:5,
      fontFamily: 'gochi'
    },
    containerLogo: {
      justifyContent: 'center',
      alignItems: 'center',
    },
    logo:{
      width:130,
      height:150,
    },
  });