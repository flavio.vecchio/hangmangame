import React from 'react';
import { Platform } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import COLORS from '../../../constants/colors';
import styles from './styles'

import Configuration, { title as configurationTitle } from '../../../screens/configuration'
import Dictionary, { title as dictionaryTitle } from '../../../screens/configuration/screens/dictionary';

const Stack = createNativeStackNavigator();

const ConfigurationNavigator = () => (
    <Stack.Navigator
      screenOptions={{
        headerStyle: styles.header,
        headerTintColor: Platform.OS === 'android' ? 'white' : COLORS.blue,
        headerTitleStyle: {
            fontFamily: "gochi",
            fontSize: 25,
        }
      }}
      initialRouteName="Configuration"
    >
      <Stack.Screen
        name="Configuration"
        component={Configuration}
        options={({ route }) => ({
          title: configurationTitle(),
        })}
      />
      <Stack.Screen
        name="Configuration/Dictionary"
        component={Dictionary}
        options={({ route }) => ({
          title: dictionaryTitle(),
        })}
      />
    </Stack.Navigator>
);

export default ConfigurationNavigator;