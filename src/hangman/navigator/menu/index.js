import React from 'react';
import { View, Text } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Ionicons } from '@expo/vector-icons';


import ConfigurationNavigator from '../configurationNavigator';
import GameNavigator from '../gameNavigator';
import HomeNavigator from '../homeNavigator';
import ProfileNavigator from '../profileNavigator';

import styles from './styles'
import COLORS from '../../../constants/colors'

const BottomTabs = createBottomTabNavigator();

const MenuNavigator = () => {
  return (
    <BottomTabs.Navigator
      screenOptions={{
        headerShown: false,
        tabBarStyle: styles.tabBar,
        tabBarShowLabel: false,
      }}
    >
      <BottomTabs.Screen
        name="HomeTab"
        component={HomeNavigator}
        options={{
          tabBarIcon: ({ focused }) => (
            <View style={styles.item}>
              <Ionicons name="md-home" size={24} color={focused ? COLORS.primary : 'black'} />
              <Text>Home</Text>
            </View>
          )
        }}
      />
      <BottomTabs.Screen
        name="ConfigurationTab"
        component={ConfigurationNavigator}
        options={{
          tabBarIcon: ({ focused }) => (
            <View style={styles.item}>
              <Ionicons name="md-settings-sharp" size={24} color={focused ? COLORS.primary : 'black'} />
              <Text>Configuracion</Text>
            </View>
          )
        }}
      />
      <BottomTabs.Screen
        name="GameTab"
        component={GameNavigator}
        options={{
          tabBarStyle: {display: 'none'},
          tabBarIcon: ({ focused }) => (
            <View style={styles.item}>
              <Ionicons name="md-game-controller" size={24} color={focused ? COLORS.primary : 'black'} />
              <Text>Jugar</Text>
            </View>
          )
        }}
      />
      <BottomTabs.Screen
        name="ProfileTab"
        component={ProfileNavigator}
        options={{
          tabBarStyle: {display: 'none'},
          tabBarIcon: ({ focused }) => (
            <View style={styles.item}>
              <Ionicons name="md-person" size={24} color={focused ? COLORS.primary : 'black'} />
              <Text>Perfil</Text>
            </View>
          )
        }}
      />
    </BottomTabs.Navigator>
  )
}


export default MenuNavigator;