import { StyleSheet } from 'react-native';
import Colors from '../../../constants/colors'

export default StyleSheet.create({
  
    tabBar: {
      position: 'absolute',
      bottom: 25,
      left: 5,
      right: 5,
      borderRadius: 15,
      height: 90,
      shadowColor: '#7f5df0',
      shadowOffset: { width: 0, height: 10 },
      shadowOpacity: 0.25,
      shadowRadius: 0.25,
      elevation: 5,
    },
    item: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
    },
    header: {
      backgroundColor: Platform.OS === 'android' ? Colors.blue : 'white',
    },
    container: {
      flex: 1,
      backgroundColor: Colors.white,
      paddingTop:40,
    },
    buttonContainer:{
      flexDirection: 'column',
      marginBottom: 20,
      paddingRight: 0,
      paddingLeft: 0,
      paddingTop:20,
    },
    title:{
      fontSize: 20,
      marginVertical: 10,
      marginLeft:5,
      fontFamily: 'gochi'
    },
    containerLogo: {
      justifyContent: 'center',
      alignItems: 'center',
    },
    logo:{
      width:130,
      height:150,
    },
  });