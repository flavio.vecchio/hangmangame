import React from 'react';
import { Platform, TouchableOpacity } from 'react-native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import COLORS from '../../../constants/colors';
import styles from './styles'
import { Ionicons } from '@expo/vector-icons';
import Profile, { title as profileTitle } from '../../../screens/profile';
import { useDispatch } from 'react-redux';
import { clearTimer } from '../../../store/actions/gameState.action';

const Stack = createNativeStackNavigator();

const GameNavigator = () => {
  const dispatch = useDispatch();

  return(
    <Stack.Navigator
      screenOptions={({ navigation, route }) => ({
        headerStyle: styles.header,
        headerTintColor: Platform.OS === 'android' ? 'white' : COLORS.blue,
        headerTitleStyle: {
            fontFamily: "gochi",
            fontSize: 25,
        },        
      })}
      initialRouteName="Profile"
    >
      <Stack.Screen
        name="Profile"
        component={Profile}
        options={({ navigation, route }) => ({
          title: profileTitle(),
          headerLeft: () => (
            <TouchableOpacity style={{paddingRight: 10}} onPress={() => {
              dispatch(clearTimer());
              return (navigation.goBack());
              } }>
              <Ionicons name="arrow-back" size={24} color="white" />
            </TouchableOpacity>
          ),
        })}
      />
    </Stack.Navigator>
);
      };

export default GameNavigator;