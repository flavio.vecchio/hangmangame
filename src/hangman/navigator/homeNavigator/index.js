import React from 'react';
import { Platform } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import COLORS from '../../../constants/colors';
import styles from './styles'

import Hangman, { title as hangmanTitle } from '../../index'

const Stack = createNativeStackNavigator();

const HomeNavigator = () => (
    <Stack.Navigator
      screenOptions={{
        headerStyle: styles.header,
        headerTintColor: Platform.OS === 'android' ? 'white' : COLORS.blue,
        headerTitleStyle: {
            fontFamily: "gochi",
            fontSize: 25,
        },
      }}
      initialRouteName="Home"
    >
      <Stack.Screen
        name="Home"
        component={Hangman}
        options={{
          title: hangmanTitle(),
        }}
      />
      
    </Stack.Navigator>
);

export default HomeNavigator;