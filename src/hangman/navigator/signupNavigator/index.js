import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Signup from '../../../screens/signup';

const Stack = createNativeStackNavigator();

const SignupNavigator = () => {
  return (
    <Stack.Navigator screenOptions={{ headerShown: false }}>
      <Stack.Screen name="Signup" component={Signup} />
    </Stack.Navigator>
  )
}

export default SignupNavigator;