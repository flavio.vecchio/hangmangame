
import HangmanLayout from './layout';
import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { loadDictionary } from '../store/actions/dictionary.action';
import { updateWordToGuess } from '../store/actions/gameState.action';
import { ejemploDePalabras } from '../constants/ejemploDePalabras';


export function title() {
  return 'Juego del ahorcado';
}



export default function Hangman( { navigation }) {
  
  const dispatch = useDispatch();

  useEffect(() => {
    initialize(dispatch);
  });

  
  return (
      <HangmanLayout/>
  );
}

const initialize = (dispatch) => {
  dispatch(loadDictionary(ejemploDePalabras));
}

