
import React from 'react';
import { Text, View,Image, Button } from 'react-native';
import styles from './styles';
import Game from '../screens/game';
import Configuration from '../screens/configuration';

function HangmanLayout() {
  
  const pantallaPrincipal = (
    <View >
      <View style={styles.containerLogo} >
        <Image style={styles.logo} source={require('../../assets/images/Hangman/hg_completo.png')}/>
      </View>
      
    </View>
  );


  return (
    <View style={styles.container}>
      {pantallaPrincipal}
    </View>
  );
}

export default HangmanLayout;