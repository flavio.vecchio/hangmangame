import React, { useEffect } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { useSelector } from 'react-redux';

import MenuNavigator from './navigator/menu';
import LoginNavigator from './navigator/loginNavigator';

const HangmanNavigator = () =>{ 

  const userLogged = useSelector(state => state.UserState.userLogged);

  useEffect(() => {
    if (userLogged) {} // dispatch de la acción de lookup
  }, [userLogged]);
  
  return(
    <NavigationContainer>
      {userLogged
        ? <MenuNavigator />
        : <LoginNavigator />
      }
    </NavigationContainer>
  )
};

export default HangmanNavigator;