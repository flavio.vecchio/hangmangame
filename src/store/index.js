import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import Reactotron from './ReactotronConfig'
import thunk from 'redux-thunk';

import Words from './reducers/dictionary.reducer';
import GameState from './reducers/gameState.reducer';
import UserState from './reducers/userData.reducer';

const RootReducer = combineReducers({
  Words,
  GameState,
  UserState,
});

const middleware = [thunk];
let composed = applyMiddleware(...middleware);
const createdEnhancer = Reactotron.createEnhancer();

composed = compose(
  applyMiddleware(...middleware),
  createdEnhancer
);

const createAppropriateStore = __DEV__ ? createStore(RootReducer,composed) : createStore(RootReducer, applyMiddleware(thunk));

export default createAppropriateStore;