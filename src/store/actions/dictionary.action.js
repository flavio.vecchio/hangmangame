export const ADD_WORD = 'ADD_WORD';
export const REMOVE_WORD = 'REMOVE_WORD';
export const UPDATE_USE_ANY_WORD = 'UPDATE_USE_ANY_WORD';
export const UPDATE_USE_SELECTED_WORD_ONLY = 'UPDATE_USE_SELECTED_WORD_ONLY';
export const LOAD_DICTIONARY = 'LOAD_DICTIONARY';
export const UPDATE_WORD = 'UPDATE_WORD';

export const addWord = (word) => ({
  type: ADD_WORD,
  payload: word,
});

export const removeWord = (word) => ({
  type: REMOVE_WORD,
  payload: word,
});

export const updateUseAnyWord = (booleanValue) => ({
    type: UPDATE_USE_ANY_WORD,
    payload: booleanValue,
});

export const updateUseSelectedWordOnly = (booleanValue) => ({
    type: UPDATE_USE_SELECTED_WORD_ONLY,
    payload: booleanValue,
});

export const loadDictionary = (words) => ({
    type: LOAD_DICTIONARY,
    payload: words,
  });

export const updateWord = (word) => ({
    type: UPDATE_WORD,
    payload: word,
});