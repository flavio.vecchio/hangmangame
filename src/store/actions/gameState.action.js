export const UPDATE_WORD_TO_GUESS = 'UPDATE_WORD_TO_GUESS';
export const UPDATE_LETTERS_GUESSED = 'UPDATE_LETTERS_GUESSED';
export const UPDATE_ERRORS = 'UPDATE_ERRORS';
export const UPDATE_SECONDS = 'UPDATE_SECONDS';
export const UPDATE_MINUTES = 'UPDATE_MINUTES';
export const UPDATE_START_TIMER = 'UPDATE_START_TIMER';
export const ADD_ONE_SECOND = 'ADD_ONE_SECOND';
export const ADD_ONE_MINUTE = 'ADD_ONE_MINUTE';
export const SET_TIMER = 'SET_TIMER';
export const CLEAR_TIMER = 'CLEAR_TIMER';
export const CLEAR_GAME = 'CLEAR_GAME';

export const addOneSecond = () => ({
    type: ADD_ONE_SECOND,
    payload: {},
});

export const addOneMinute = () => ({
    type: ADD_ONE_MINUTE,
    payload: {},
});

export const updateWordToGuess = (word) => ({
    type: UPDATE_WORD_TO_GUESS,
    payload: word,
});

export const setTimer = (aTimerFunction) => ({
    type: SET_TIMER,
    payload: aTimerFunction,
});

export const clearTimer = () => ({
    type: CLEAR_TIMER,
    payload: {},
});

export const clearGame = () => ({
    type: CLEAR_GAME,
    payload: {},
});

export const updateLettersGuessed = (letter,index) => ({
    type: UPDATE_LETTERS_GUESSED,
    payload: {letter,index},
  });

export const updateErrors = (error) => ({
    type: UPDATE_ERRORS,
    payload: error,
});

export const updateSeconds = (second) => ({
    type: UPDATE_SECONDS,
    payload: second,
});

export const updateMinutes = (minute) => ({
    type: UPDATE_MINUTES,
    payload: minute,
});

export const updateStartTimer = (boolean) => ({
    type: UPDATE_START_TIMER,
    payload: boolean,
});