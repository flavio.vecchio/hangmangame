import { version } from "react-dom";
import { SIGN_IN_URL, SIGN_UP_URL, API_URL } from "../../constants/endpoints";
import { insertConfig, fetchConfig } from "../../db";

export const LOGIN_OK = 'LOGIN_OK';
export const LOGOUT_OK = 'LOGOUT_OK';
export const SAVE_IMAGE ='SAVE_IMAGE';
export const SIGN_IN = 'SIGN_IN';
export const SIGN_OUT = 'SIGN_OUT';
export const HIDE_LOGIN_FAIL = 'HIDE_LOGIN_FAIL';
export const LOGIN_FAIL = 'LOGIN_FAIL';
export const SIGN_UP = 'SIGN_UP';

//https://hangmanbackend-8ea4a-default-rtdb.firebaseio.com/users/zPtg8yToj2hflVcsW0JZW2lqOU43.json?auth=XtJQ7vW8kya1gjR6Yd7Y0PMue5r0NOGzvsHWgBPP

const getUserInfoFromFB = async (uid) => {
    
    try {
      const response = await fetch(`${API_URL}/users/${uid}.json?print=pretty&auth=XtJQ7vW8kya1gjR6Yd7Y0PMue5r0NOGzvsHWgBPP`, {
        method: 'GET',
        header: {
          'Content-Type': 'application/json',
        },
    
      }).then(response=>response.json())
      ;

      const data = response['configuracion'];
      
      return data;
      

    } catch(error) {
       console.log(error.message);
    }
  
};

const addNewUser = async (uid, image, email, version) => {
    
        try {
          const response = await fetch(`${API_URL}/users/${uid}.json?auth=XtJQ7vW8kya1gjR6Yd7Y0PMue5r0NOGzvsHWgBPP`, {
            method: 'PUT',
            header: {
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                'configuracion':{
                    avatar: image,
                    email,
                    version,
                }
              
            }),
          });
    
          //console.log("Response NEW USER: "+JSON.stringify(response));

        } catch(error) {
           console.log(error.message);
        }
      
};

export const signupNewUser = (email, password, image) => {
    return async dispatch => {
        try {
          const response = await fetch(SIGN_UP_URL, {
            method: 'POST',
            header: {
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({
              email,
              password,
              returnSecureToken: true,
            }),
          });
    
          const data = await response.json();
        

          let result;

          if(response.status !== 200){
              result = {
                  type: LOGIN_FAIL,
                  errorMessage: data.error.message,
              };
              return dispatch(result);

          }
             result = {
                type: SIGN_IN,
                token: data.idToken,
                userId: data.localId,
                avatar: image,
                version: 0,
              }
          
          await addNewUser(data.localId,image,email,0);

          const resultDB = await insertConfig(data.localId,email,image,0);
          //console.log(resultDB);
    
          dispatch(result);

        } catch(error) {
           console.log(error.message);
        }
      }
};


export const signIn = (email, password) => {
    return async dispatch => {
        try {
          const responseAUTH = await fetch(SIGN_IN_URL, {
            method: 'POST',
            header: {
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({
              email,
              password,
              returnSecureToken: true,
            }),
          });
    
          const data = await responseAUTH.json();
          let result;

          if(responseAUTH.status !== 200){
              result = {
                  type: LOGIN_FAIL,
              };
          }else{
             result = {
                type: SIGN_IN,
                token: data.idToken,
                userId: data.localId,
                userName: email,
              }
          }
    
          const dataFromFB = await getUserInfoFromFB(data.localId);
          const dataFromDB = await fetchConfig(data.localId);

          const configurationFromDB = dataFromDB.rows._array[0];
          const configurationFromFB = dataFromFB;

          //console.log("INFO FROM DB:"+JSON.stringify(configurationFromDB));
          //console.log("INFO FROM FB:"+JSON.stringify(configurationFromFB));

          //si la version de SQL y FB es igual, me quedo con alguna, es indistinto
          if (configurationFromDB && configurationFromFB && configurationFromDB.version === configurationFromFB.version){
            console.log("La configuracion es igual, tomo la de FB")
            result = {
                type: SIGN_IN,
                token: data.idToken,
                userId: data.localId,
                userName: configurationFromFB.email,
                avatar: configurationFromFB.avatar,
                version: configurationFromFB.version,
              }
          }
          
          //si la configuracion de SQL es menor a la de FB me quedo con la de FB
          if (configurationFromDB && configurationFromFB && configurationFromDB.version < configurationFromFB.version){
            console.log("La configuracion valida es la de FB")
            result = {
                type: SIGN_IN,
                token: data.idToken,
                userId: data.localId,
                userName: configurationFromFB.email,
                avatar: configurationFromFB.avatar,
                version: configurationFromFB.version,
              }
          }
          
          //si la configuracion de SQL es mayor a la de FB me quedo con la de SQL
          if (configurationFromDB && configurationFromFB && configurationFromDB.version > configurationFromFB.version){
            console.log("La configuracion valida es la de DB")
            result = {
                type: SIGN_IN,
                token: data.idToken,
                userId: data.localId,
                userName: configurationFromDB.email,
                avatar: configurationFromDB.avatar,
                version: configurationFromDB.version,
              }
          }

          //contemplar caso cuando no hay configuracion valida como error


          dispatch(result);

        } catch(error) {
           console.log(error.message);
        }
      }
};

export const hideLoginFail = () => ({
    type: HIDE_LOGIN_FAIL,
    payload: {},
});

export const loginOk = () => ({
    type: LOGIN_OK,
    payload: {},
});

export const logoutOk = () => ({
    type: LOGOUT_OK,
    payload: {},
});

export const singout = () => ({
  type: SIGN_OUT,
  payload: {},
});

export const saveImage = (image) => ({
    type: SAVE_IMAGE,
    payload: {image},
});

