import { 
    LOGIN_OK,
    LOGOUT_OK,
    SAVE_IMAGE,
    SIGN_IN,
    SIGN_OUT,
    HIDE_LOGIN_FAIL,
    LOGIN_FAIL,
} from '../actions/userData.action';

const initialState = {
  userName:'',
  userLogged:false,
  avatar: undefined,
  idToken: '',
  localId: '',
  showLogginFail: false,
  errorMessage: '',
  version: 0,
};

const UserState = (state = initialState, action) => {
  
  switch(action.type) {
    case HIDE_LOGIN_FAIL:
        return {
            ...state,
            showLogginFail: false,
            errorMessage: '',
        };
    case LOGIN_OK:
        return {
            ...state,
            userLogged: true,
        };
    case LOGOUT_OK:
        return {
            ...state,
            userLogged: false,
        };
    case SAVE_IMAGE:
      return {
          ...state,
          avatar: action.payload.image,
      };
    case SIGN_IN:
      return {
          ...state,
          userLogged: true,
          idToken: action.token,
          localId: action.userId,
          showLogginFail: false,
          errorMessage: '',
          avatar: action.avatar,
          userName: action.userName,
          version: action.version,
      };
    case SIGN_OUT:
        return {
            ...state,
            userLogged: false,
            idToken: '',
            localId: '',
            userName: '',
            version: 0,
            showLogginFail: false,
            errorMessage: '',
      };
    case LOGIN_FAIL:
      return {
          ...state,
          showLogginFail: true,
          errorMessage: action.errorMessage,
    };
    default:
      return state;
  }
};

export default UserState;