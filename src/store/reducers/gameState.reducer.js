import { 
    UPDATE_WORD_TO_GUESS,
    UPDATE_LETTERS_GUESSED,
    UPDATE_ERRORS,
    UPDATE_SECONDS,
    UPDATE_MINUTES,
    UPDATE_START_TIMER,
    ADD_ONE_MINUTE,
    ADD_ONE_SECOND,
    SET_TIMER,
    CLEAR_TIMER,
    CLEAR_GAME,
} from '../actions/gameState.action';

const initialState = {
  palabraAAdivinar: '',
  letrasAdivinadas:[],
  errores: 0,
  segundos: 0,
  minutos: 0,
  startTimer: false,
  timer: undefined,
};

const GameState = (state = initialState, action) => {
  switch(action.type) {
    case CLEAR_GAME:
        return {
            ...initialState,
        };
    case ADD_ONE_SECOND:
        return {
            ...state,
            segundos: state.segundos+1,
        };
    case ADD_ONE_MINUTE:
        return {
            ...state,
            minutos: state.minutos+1,
        };
    case UPDATE_WORD_TO_GUESS:
        return {
            ...state,
            palabraAAdivinar: action.payload,
        };
    case SET_TIMER:
        return {
            ...state,
            timer: action.payload,
        };
    case CLEAR_TIMER:
        const timer = state.timer;
        if(timer){
            clearInterval(timer)
        }
        return {
            ...state,
            timer: undefined,
            startTimer: false,
        };
    case UPDATE_LETTERS_GUESSED:
        const letrasAdivinadas = [...state.letrasAdivinadas];
        letrasAdivinadas[action.payload.index]=action.payload.letter;

        return {
            ...state,
            letrasAdivinadas,
        };
    case UPDATE_ERRORS:
        return {
            ...state,
            errores: action.payload,
        };
    case UPDATE_SECONDS:
        return {
            ...state,
            segundos: action.payload,
        };
    case UPDATE_MINUTES:
        return {
            ...state,
            minutos: action.payload,
        };
    case UPDATE_START_TIMER:{
        return {
            ...state,
            startTimer: action.payload,
        };
    }
                
    default:
      return state;
  }
};

export default GameState;