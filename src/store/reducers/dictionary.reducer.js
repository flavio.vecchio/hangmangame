import { 
    ADD_WORD, 
    REMOVE_WORD, 
    UPDATE_USE_SELECTED_WORD_ONLY, 
    UPDATE_USE_ANY_WORD,
    LOAD_DICTIONARY,
    UPDATE_WORD,
} from '../actions/dictionary.action';

const initialState = {
  words: [],
  useAnyWord: true,
  useSelectedWordOnly: false,
};

const Words = (state = initialState, action) => {
  switch(action.type) {
    case ADD_WORD:
        return {
            ...state,
            words: [...state.words, action.payload],
        };
    case REMOVE_WORD:
        return {
            ...state,
            words: state.words.filter(word => word.id !== action.payload.id),
        };
    case UPDATE_USE_SELECTED_WORD_ONLY:
        return {
            ...state,
            useSelectedWordOnly: action.payload,
        };
    case UPDATE_USE_ANY_WORD:
        return {
            ...state,
            useAnyWord: action.payload,
        };
    case LOAD_DICTIONARY:
        return {
            ...state,
            words: action.payload,
        };
    case UPDATE_WORD:{
        const colPalabras = [...state.words];
        const index = colPalabras.indexOf( word => word.id === action.payload.id);
        if(index !== -1){
            colPalabras[index] = action.payload;
        }

        return {
            ...state,
            words: colPalabras,
        };
    }
                
    default:
      return state;
  }
};

export default Words;