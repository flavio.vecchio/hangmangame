// Brand Colors
exports.darkBlue = '#225380';
exports.nepal = '#91A9C0';

exports.blue = '#01A2E3';
exports.lightBlue = '#A7DFF6';

// Another products colors
exports.green = '#57DFB7';
exports.greenDisabled = '#B2E6D4';
exports.yellow = '#FFDF00';
exports.red = '#F43D3D';
exports.darkYellow = '#FFCA00';

// Grays
exports.dirtyWhite = '#FAFBF9';
exports.lightGray = '#ECEDE9';
exports.gray = '#DBDCD7';
exports.darkGray = '#6B6B6B';
exports.darkestGray = '#4A4A4A';

// Other colors
exports.white = '#FFF';
exports.black = '#000';
exports.transparent = 'transparent';
exports.transparentGray = 'rgba(0, 0, 0, 0.38)';

exports.primary = '#2D93AD';
exports.accent = '#aac0af';
